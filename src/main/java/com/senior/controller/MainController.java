package com.senior.controller;

import com.senior.model.*;
import com.senior.service.*;
import net.sf.jasperreports.engine.JRException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Date;
import java.util.List;

@RestController
@CrossOrigin("*")
@RequestMapping("/")
public class MainController {

    @Autowired
    AuthenService authenService;

    @Autowired
    UserService userService;

    @Autowired
    ProcessService processService;

    @Autowired
    BillProcessService billProcessService;

    @Autowired
    ProductService productService;

    @Autowired
    DepartmentService departmentService;

    @Autowired
    SummaryService summaryService;

    @PostMapping("login")
    @ResponseBody
    public LoginResponse validateLogin(@RequestBody LoginRequest loginRequest) {
        return authenService.validateLogin(loginRequest.getUserName(), loginRequest.getPassword());
    }

    @PostMapping("createUser")
    @ResponseBody
    public void createUser(@RequestBody UserRequest userRequest){
        userService.createUser(userRequest);
    }

    @GetMapping("getAllUser")
    @ResponseBody
    public List<UserResponse> getAllUser(){
        return userService.getAll();
    }

    @PutMapping("/resetPassword/{id}")
    @ResponseBody
    public void createUser(@PathVariable Integer id){
        userService.resetPassword(id);
    }

    @DeleteMapping("/deleteUser/{id}")
    @ResponseBody
    public void deleteUser(@PathVariable Integer id){
        userService.deleteById(id);
    }

    @PostMapping("/process")
    @ResponseBody
    public void createProcess(@RequestBody List<ProcessRequest> processRequest){
        processService.create(processRequest);
    }

//    @GetMapping("/getBillingProcess/{date}")
//    @ResponseBody
//    public  List<BillingResponse> createProcess(@PathVariable Date date){
//        return processService.getByDate(date);
//    }

    @GetMapping(value = "/generateBill/{poId}")
    public byte[] generateBill(@PathVariable Integer poId) throws JRException {

        return billProcessService.generateBill(poId);
    }

    @GetMapping("/getPoByDepartment/{id}")
    @ResponseBody
    public List<PoResponse> getPoByDepartment(@PathVariable Integer id){
        return processService.getPoByDepartment(id);
    }

    @GetMapping("/getProcessByPo/{id}")
    @ResponseBody
    public List<ProcessResponse> getProcessByPo(@PathVariable Integer id){
        return processService.getByPoId(id);
    }

    @PostMapping("/editProcess")
    @ResponseBody
    public void editProcess(@RequestBody List<ProcessRequest> processRequest){
        processService.editProcess(processRequest);
    }

    @DeleteMapping("/deletePo/{id}")
    @ResponseBody
    public void deletePo(@PathVariable Integer id){
        processService.deleteByPoId(id);
    }

    @RequestMapping(value = "/uploadFile", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public void  myService(@RequestParam("file") MultipartFile file, @RequestParam("departmentId") Integer departmentId) throws IOException {
        productService.storeFile(file, departmentId);
    }

    @GetMapping("/getAllProduct")
    public List<ProductResponse> getProduct(){
        return productService.getFile();
    }

    @GetMapping("/getProductByDepartmentId/{id}")
    public List<ProductResponse> getProduct(@PathVariable Integer id){
        return productService.getProductByDepartmentId(id);
    }

    @DeleteMapping("/deleteProduct/{id}")
    @ResponseBody
    public void deleteProduct(@PathVariable Integer id){
        productService.deleteById(id);
    }

    @PostMapping("/createDepartment")
    @ResponseBody
    public void createDepartment(@RequestBody DepartmentRequest departmentRequest){
        departmentService.create(departmentRequest);
    }

    @DeleteMapping("/deleteDepartment/{id}")
    @ResponseBody
    public void deleteDepartment(@PathVariable Integer id){
        departmentService.deleteById(id);
    }

    @GetMapping("/getAllDepartment")
    public List<DepartmentResponse> getAllDepartment(){
        return departmentService.getAll();
    }

    @GetMapping("/getBillingProcess/{departmentId}/{startDate}/{endDate}")
    @ResponseBody
    public  List<SumaryResponse> getSummary(@PathVariable Integer departmentId, @PathVariable Date startDate, @PathVariable Date endDate){
        return summaryService.getSummary(departmentId, startDate, endDate);
    }

}
