package com.senior.model;

import com.senior.entity.User;

import java.sql.Timestamp;

public class UserResponse {

    private Integer row;
    private Integer id;
    private String username;
    private String password;
    private String role;
    private String created;
    private String name;
    private Boolean firstLogin;
    private String department;

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getFirstLogin() {
        return firstLogin;
    }

    public void setFirstLogin(Boolean firstLogin) {
        this.firstLogin = firstLogin;
    }

    public Integer getRow() {
        return row;
    }

    public void setRow(Integer row) {
        this.row = row;
    }

//    public UserResponse(User user){
//        this.setId(user.getId());
//        this.setName(user.getName());
//        this.setUsername(user.getUsername());
//        this.setRole(user.getRole());
//        this.setFirstLogin(user.getFirstLogin());
//        this.setCreated(user.getCreated());
//    }
}
