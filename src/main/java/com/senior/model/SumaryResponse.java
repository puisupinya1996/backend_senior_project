package com.senior.model;

import java.util.Date;
import java.util.List;

public class SumaryResponse {

    private Integer row;
    private String poNo;
    private String invoiceNo;
    private String processDate;
    private List<ProcessResponse> classList;

    public String getPoNo() {
        return poNo;
    }

    public void setPoNo(String poNo) {
        this.poNo = poNo;
    }

    public String getInvoiceNo() {
        return invoiceNo;
    }

    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    public List<ProcessResponse> getClassList() {
        return classList;
    }

    public void setClassList(List<ProcessResponse> classList) {
        this.classList = classList;
    }

    public Integer getRow() {
        return row;
    }

    public void setRow(Integer row) {
        this.row = row;
    }

    public String getProcessDate() {
        return processDate;
    }

    public void setProcessDate(String processDate) {
        this.processDate = processDate;
    }
}
