package com.senior.repository;

import com.senior.entity.Department;
import com.senior.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository extends JpaRepository<Product, Integer> {
    public List<Product> findByDepartmentId(Integer departmentId);
}
