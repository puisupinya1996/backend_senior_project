package com.senior.repository;

import com.senior.entity.Process;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;

@Repository
public interface ProcessRepository extends JpaRepository<Process, Integer> {

    List<Process> findByPoNo(String poNo);
    List<Process> findAllByPoId(Integer poId);

    Long deleteByPoId(Integer poId);

    List<Process> findByPoId(Integer poId);

}
