package com.senior.repository;

import com.senior.entity.Preorder;
import com.senior.entity.Process;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;

@Repository
public interface PreorderRepository extends JpaRepository<Preorder, Integer> {

    @Query(value = "SELECT * FROM preorder WHERE process_date BETWEEN :startDate AND :endDate", nativeQuery = true)
    public List<Preorder> getAllBetweenDates(@Param("startDate") Date startDate, @Param("endDate") Date endDate);

    public List<Preorder> findByDepartmentId(Integer departmentId);

    @Transactional
    Long deleteByPoNo(String poNo);

    public List<Preorder> findByPoNo(String poNo);

    @Query(value = "SELECT * FROM preorder WHERE department_id = :departmentId AND process_date BETWEEN :startDate AND :endDate", nativeQuery = true)
    public List<Preorder> getAllBetweenDatesAndDepartmentId(@Param("departmentId") Integer id,@Param("startDate") Date startDate, @Param("endDate") Date endDate);

}
