package com.senior.service;

import com.senior.entity.Preorder;
import com.senior.entity.Process;
import com.senior.model.ProcessResponse;
import com.senior.model.SumaryResponse;
import com.senior.repository.PreorderRepository;
import com.senior.repository.ProcessRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class SummaryService {

    private static SimpleDateFormat formatDate = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

    @Autowired
    ProcessRepository processRepository;

    @Autowired
    PreorderRepository preorderRepository;

    public byte[] generateBill(Integer departmentId, Date startDate, Date endDate) {
        List<Preorder> preOrderList = preorderRepository.getAllBetweenDatesAndDepartmentId(departmentId, startDate, endDate);
        List<SumaryResponse> sumaryResponseList = pepareSummary(departmentId, startDate, endDate);

        return null;
    }

    public List<SumaryResponse> getSummary(Integer departmentId, Date startDate, Date endDate){
        return pepareSummary(departmentId, startDate, endDate);
    }

    private List<SumaryResponse> pepareSummary(Integer departmentId, Date startDate, Date endDate) {

        List<Preorder> preOrderList = preorderRepository.getAllBetweenDatesAndDepartmentId(departmentId, startDate, endDate);
        List<SumaryResponse> sumaryResponseList = new ArrayList<>();
        int x =1 ;
        for (Preorder preorder : preOrderList) {
            SumaryResponse summary = new SumaryResponse();
            summary.setRow(x);
            summary.setPoNo(preorder.getPoNo());
            summary.setInvoiceNo(preorder.getInvoiceNo());
            summary.setProcessDate(formatDate.format(preorder.getProcessDate()));
            List<Process> processList = processRepository.findByPoNo(preorder.getPoNo());
            List<ProcessResponse> processResponseList = processList.stream().map(p -> {
                ProcessResponse process = new ProcessResponse();
                process.setPartNo(p.getPartNo());
                process.setInput(p.getInput());
                process.setOutput(p.getOutput());
                process.setUnitPrice(p.getUnitPrice());
                process.setNetAmount(p.getNetAmount());
                return process;
            }).collect(Collectors.toList());
            summary.setClassList(processResponseList);

            sumaryResponseList.add(summary);
            x++;
        }
        return sumaryResponseList;
    }
}
