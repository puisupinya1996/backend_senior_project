package com.senior.service;

import com.senior.entity.Department;
import com.senior.entity.Product;
import com.senior.model.ProductResponse;
import com.senior.repository.DepartmentRepository;
import com.senior.repository.ProductRepository;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProductService {

    @Autowired
    ProductRepository productRepository;

    @Autowired
    DepartmentRepository departmentRepository;

    public void storeFile(MultipartFile file, Integer departmentId) throws IOException {
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());

        Product productFile = new Product();
        productFile.setPartNo(fileName.split("\\.")[0]);
        productFile.setFileType(file.getContentType());
        productFile.setImage(file.getBytes());
        productFile.setDepartmentId(departmentId);
        productRepository.save(productFile);

    }

    public List<ProductResponse> getFile(){

        List<Department> departmentList = departmentRepository.findAll();
        List<Product> productList = productRepository.findAll();
        List<ProductResponse> prsl = new ArrayList<>();
        for(Product p: productList){
            String b = Base64.getEncoder().encodeToString(p.getImage());
            ProductResponse ps = new ProductResponse();
            ps.setId(p.getId());
            ps.setPartNo(p.getPartNo());
            ps.setImageBase64(b);
            List<String> names =
                    departmentList.stream()
                            .filter(d -> d.getId().equals(p.getDepartmentId()))
                            .map(Department::getName)
                            .collect(Collectors.toList());
            if(names.size()>0) {
                ps.setDepartmentName(names.get(0).toString());
            }
            prsl.add(ps);
        }
        return prsl;
    }

    public List<ProductResponse> getProductByDepartmentId(Integer departmentId){

        List<Department> departmentList = departmentRepository.findAll();
        List<Product> productList = new ArrayList<>();
        if(departmentId > 0){
            productList = productRepository.findByDepartmentId(departmentId);
        }else{
            productList = productRepository.findAll();
        }

        List<ProductResponse> prsl = new ArrayList<>();
        for(Product p: productList){
            String b = Base64.getEncoder().encodeToString(p.getImage());
            ProductResponse ps = new ProductResponse();
            ps.setId(p.getId());
            ps.setPartNo(p.getPartNo());
            ps.setImageBase64(b);
            ps.setDepartmentId(p.getDepartmentId());
            List<String> names =
                    departmentList.stream()
                            .filter(d -> d.getId().equals(p.getDepartmentId()))
                            .map(Department::getName)
                            .collect(Collectors.toList());
            if(names.size()>0) {
                ps.setDepartmentName(names.get(0).toString());
            }
            prsl.add(ps);
        }
        return prsl;
    }

    public void deleteById(Integer id){
        productRepository.deleteById(id);
    }

}
