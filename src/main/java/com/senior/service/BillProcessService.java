package com.senior.service;

import com.senior.entity.Preorder;
import com.senior.entity.Process;
import com.senior.repository.PreorderRepository;
import com.senior.repository.ProcessRepository;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@Service
public class BillProcessService {

    @Autowired
    ProcessRepository processRepository;

    @Autowired
    PreorderRepository preorderRepository;

    private static SimpleDateFormat formatDate = new SimpleDateFormat("dd/MM/yyyy");

    public byte[] generateBill(Integer poId)  {
        byte[] b = null;
        try {
            Optional<Preorder> preOrder = preorderRepository.findById(poId);
            if(preOrder.isPresent()) {
                List<Process> processList = processRepository.findAllByPoId(poId);

                double amount = 0.0;
                for (Process p : processList) {
                    amount += p.getNetAmount();
                }

                double vat = (amount*7)/100;
                HashMap<String, Object> param = new HashMap<String, Object>();
                param.put("processDate", formatDate.format(preOrder.get().getProcessDate()));
                param.put("poNo", preOrder.get().getPoNo());
                param.put("invoiceNo", preOrder.get().getInvoiceNo());
                param.put("amount", amount);
                param.put("vat", vat);
                param.put("totalAmount", amount + vat);
                String jasperFile = "src\\main\\resources\\s2_smart_invoice.jasper";
                JasperPrint jp = JasperFillManager.fillReport(jasperFile, param,
                        new JRBeanCollectionDataSource(processList));
                b = JasperExportManager.exportReportToPdf(jp);
            }
        } catch (JRException e) {
            e.printStackTrace();
        }
        return b;
    }
}
