package com.senior.service;

import com.senior.entity.Department;
import com.senior.entity.User;
import com.senior.model.CommonResponse;
import com.senior.model.UserRequest;
import com.senior.model.UserResponse;
import com.senior.repository.DepartmentRepository;
import com.senior.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserService {
    private static SimpleDateFormat formatDate = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    @Autowired
    UserRepository userRepository;

    @Autowired
    DepartmentRepository departmentRepository;

    public void createUser(UserRequest userRequest){
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        User user = new User();
        user.setUsername(userRequest.getUserName());
        user.setName(userRequest.getName());
        user.setRole(userRequest.getRole());
        user.setDepartmentId(userRequest.getDepartment());
        user.setPassword("123456789");
        user.setCreated(timestamp);
        userRepository.save(user);
    }

    public List<UserResponse> getAll(){
        List<User> userList = userRepository.findAll();
        List<UserResponse> userResponses = new ArrayList<>();
        int x= 1;
        for(User user: userList){
            UserResponse userResponse = new UserResponse();
            userResponse.setRow(x);
            userResponse.setId(user.getId());
            userResponse.setName(user.getName());
            userResponse.setUsername(user.getUsername());
            userResponse.setRole(user.getRole());
            userResponse.setFirstLogin(user.getFirstLogin());
            userResponse.setCreated(formatDate.format(user.getCreated()));
            List<Department> departments = departmentRepository.findAll();
            Department department = departments.stream().filter(d -> d.getId() == user.getDepartmentId()).findFirst().orElse(null);
            userResponse.setDepartment("ALL");
            if(department != null){
                userResponse.setDepartment(department.getName());
            }
            userResponses.add(userResponse);
            x++;
        }
        return userResponses;
    }

    public void resetPassword(Integer id){
        User user = userRepository.getOne(id);
        user.setPassword("123456789");
        user.setFirstLogin(true);
        userRepository.save(user);
    }

    public void deleteById(Integer id){
        userRepository.deleteById(id);
    }

    public User findById(Integer id){
        return userRepository.getOne(id);
    }

}
