package com.senior.service;

import com.senior.entity.User;
import com.senior.model.LoginResponse;
import com.senior.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AuthenService {

    @Autowired
    UserRepository userRepository;

    public LoginResponse validateLogin(String username, String password){

        List<User> users = userRepository.findByUsernameAndPassword(username, password);
        LoginResponse loginResponse = new LoginResponse();
        if( users.size() > 0){
            loginResponse.setHttpStatus(HttpStatus.OK);
            loginResponse.setMessage(users.get(0).getId()+ "|" + users.get(0).getName() + "|" + users.get(0).getUsername() + "|" + users.get(0).getRole()+ "|" + users.get(0).getDepartmentId());
        }else{
            loginResponse.setHttpStatus(HttpStatus.UNAUTHORIZED);
        }
        return loginResponse;
    }
}
