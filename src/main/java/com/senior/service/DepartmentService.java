package com.senior.service;

import com.senior.entity.Department;
import com.senior.model.DepartmentRequest;
import com.senior.model.DepartmentResponse;
import com.senior.repository.DepartmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class DepartmentService {

    @Autowired
    DepartmentRepository departmentRepository;

    public void create(DepartmentRequest departmentRequest){
        Department d = new Department();
        d.setName(departmentRequest.getName());
        departmentRepository.save(d);
    }

    public void deleteById(Integer id){
        departmentRepository.deleteById(id);
    }

    public List<DepartmentResponse> getAll() {

        List<Department> departmentList = departmentRepository.findAll();
        List<DepartmentResponse> rs = new ArrayList<>();
        for(Department d: departmentList){
            DepartmentResponse departmentRs = new DepartmentResponse();
            departmentRs.setId(d.getId());
            departmentRs.setName(d.getName());
            rs.add(departmentRs);
        }
        return rs;
    }

}
