package com.senior.service;


import com.senior.entity.Preorder;
import com.senior.entity.Process;
import com.senior.model.BillingResponse;
import com.senior.model.PoResponse;
import com.senior.model.ProcessRequest;
import com.senior.model.ProcessResponse;
import com.senior.repository.PreorderRepository;
import com.senior.repository.ProcessRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static java.time.temporal.TemporalAdjusters.firstDayOfYear;
import static java.time.temporal.TemporalAdjusters.lastDayOfYear;

@Service
public class ProcessService {

    private static SimpleDateFormat formatDate = new SimpleDateFormat("dd/MM/yyyy");
    @Autowired
    ProcessRepository processRepository;

    @Autowired
    PreorderRepository preorderRepository;

    @Autowired
    UserService userService;

    @Autowired
    BillProcessService billProcessService;

    public void create(List<ProcessRequest> processRequestList){
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        List<Process> processList = new ArrayList<>();
        Process process;
        String invoiceNo = generateInvoiceNo();

        Preorder preorder = new Preorder();
        preorder.setPoNo(processRequestList.get(0).getPoNo());
        preorder.setInvoiceNo(invoiceNo);
        preorder.setProcessDate(processRequestList.get(0).getProcessDate());
        preorder.setDepartmentId(processRequestList.get(0).getDepartmentId());
        preorder.setUserId(processRequestList.get(0).getUserId());
        preorder.setCreated(timestamp);
        Preorder rs = preorderRepository.save(preorder);

        for(ProcessRequest p: processRequestList){
            process = new Process();
            process.setPoId(rs.getId());
            process.setPoNo(p.getPoNo());
            process.setPartNo(p.getPartNo());
            process.setInput(p.getInput());
            process.setOutput(p.getOutput());

            processList.add(process);
        }
        processRepository.saveAll(processList);
    }

    public void editProcess(List<ProcessRequest> processRequestList){
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        List<Preorder> preOrderList = preorderRepository.findByPoNo(processRequestList.get(0).getPoNo());
        Preorder preOrder = preOrderList.get(0);
        preOrder.setEditUserId(processRequestList.get(0).getUserId());
        preOrder.setEditTime(timestamp);
        preorderRepository.save(preOrder);
        for(ProcessRequest prq: processRequestList) {
            Process process = processRepository.getOne(prq.getId());
            process.setInput(prq.getInput());
            process.setOutput(prq.getOutput());
            process.setUnitPrice(prq.getUnitPrice());
            process.setNetAmount(prq.getNetAmount());

            processRepository.save(process);
        }
    }

//    public List<BillingResponse> getByDate(Date date){
//        List<BillingResponse> billingresp = new ArrayList<>();
//        List<Process> processList =  processRepository.findByProcessDate(date);
//        BillingResponse billing;
//        for(Process process: processList){
//            billing = new BillingResponse();
//
//            billing.setId(process.getId());
//            billing.setPartNo(process.getPartNo());
//            billing.setInput(process.getInput());
//            billing.setOutput(process.getOutput());
//            billing.setPartNo(process.getPartNo());
//            billing.setUnitCost(null);
//
//            billingresp.add(billing);
//        }
//        return billingresp;
//    }

    public List<PoResponse> getPoByDepartment(Integer departmentId){
        SimpleDateFormat formatTime = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        List<Preorder> preorderList = new ArrayList<>();
        if(departmentId != 0) {
            preorderList = preorderRepository.findByDepartmentId(departmentId);
        }else{
            preorderList = preorderRepository.findAll();
        }

        List<PoResponse> poResponses = new ArrayList<>();
        int x=1;
        for(Preorder preorder : preorderList){
            PoResponse po = new PoResponse();
            po.setRow(x);
            po.setPoId(preorder.getId());
            po.setPoNo(preorder.getPoNo());
            po.setInvoiceNo(preorder.getInvoiceNo());
            po.setProcessDate(preorder.getProcessDate() != null ?formatDate.format(preorder.getProcessDate()):"");
            po.setCreateName(userService.findById(preorder.getUserId()).getName());
            po.setCreateTime(preorder.getCreated() != null ? formatTime.format(preorder.getCreated()) : "");
            if(preorder.getEditUserId() != null) {
                po.setEditName(userService.findById(preorder.getEditUserId()).getName());
                po.setEditTime(preorder.getEditTime() != null ? formatTime.format(preorder.getEditTime()) : "");
            }
            x++;
            poResponses.add(po);
        }
        return poResponses;
//        return poResponses.stream()
//                .filter(distinctByKeys(p -> p.getPoNo()))
//                .collect(Collectors.toList());
    }

    private static <T> Predicate<T> distinctByKeys(Function<? super T, ?>... keyExtractors)
    {
        final Map<List<?>, Boolean> seen = new ConcurrentHashMap<>();

        return t ->
        {
            final List<?> keys = Arrays.stream(keyExtractors)
                    .map(ke -> ke.apply(t))
                    .collect(Collectors.toList());

            return seen.putIfAbsent(keys, Boolean.TRUE) == null;
        };
    }

    public String generateInvoiceNo(){

        LocalDate now = LocalDate.now();
        LocalDate firstDay = now.with(firstDayOfYear());
        LocalDate lastDay = now.with(lastDayOfYear());
        List<Preorder> preorderList =  preorderRepository.getAllBetweenDates(Date.valueOf(firstDay) , Date.valueOf(lastDay));
        String invoiceNo = "";
        if(preorderList.size() > 0){
            invoiceNo =  "S2" + firstDay.toString().split("-")[0] + "-C" +  StringUtils.leftPad(preorderList.get(preorderList.size()-1).getId().toString(), 5,"0");
        }else{
            invoiceNo =  "S2" + firstDay.toString().split("-")[0] + "-C00001";
        }
        return invoiceNo;
    }

    public List<ProcessResponse> getByPoId(Integer id){
        List<ProcessResponse> processRespList = new ArrayList<>();

        List<Process> processList =  processRepository.findByPoId(id);
        Optional<Preorder> preorder = preorderRepository.findById(id);
        ProcessResponse processRes;
        int x= 1;
        if(preorder.isPresent()) {
            for (Process process : processList) {
                processRes = new ProcessResponse();
                processRes.setRow(x);
                processRes.setId(process.getId());
                processRes.setPoId(preorder.get().getId());
                processRes.setPoNo(process.getPoNo());
                processRes.setInvoiceNo(preorder.get().getInvoiceNo());
                processRes.setPartNo(process.getPartNo());
                processRes.setInput(process.getInput());
                processRes.setOutput(process.getOutput());
                processRes.setUnitPrice(process.getUnitPrice());
                processRes.setNetAmount(process.getNetAmount());
                processRes.setProcessDate(preorder.get().getProcessDate());
                x++;
                processRespList.add(processRes);
            }
        }
        return processRespList;
    }

    public void deleteByPoId(Integer poId){
        preorderRepository.deleteById(poId);
        List<Process> rs = processRepository.findAllByPoId(poId);
        processRepository.deleteAll(rs);
    }

}
